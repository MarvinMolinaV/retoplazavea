import { Component, OnInit } from '@angular/core';
import { ProductoService } from './../../services/producto.service';
import { Producto } from './../../models/producto';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {
  public ctg: any;
  public productos!: Producto[];

  constructor(
    private productoService: ProductoService,
  ) { }

  ngOnInit(): void {
    this.ctg = localStorage.getItem('categoria');
    this.listarProductos();
  }

  listarProductos() {
    console.log(this.ctg);
    this.productoService.getProductos(this.ctg).subscribe(data => {
      this.productos = data;
      console.log(data);
    })
  }

}
