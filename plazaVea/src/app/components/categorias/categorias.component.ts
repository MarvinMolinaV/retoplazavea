import { Component, OnInit } from '@angular/core';
import { CategoriaService } from './../../services/categoria.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.css']
})
export class CategoriasComponent implements OnInit {

  public categorias: any;
  public ctg!: string;
  constructor(
    private categoriaService: CategoriaService,
    public router: Router
  ) { }

  ngOnInit(): void {
    localStorage.removeItem('categoria');
    this.listarCategorias();
  }

  listarCategorias() {
    this.categoriaService.getCategorias().subscribe(data => {
      this.categorias = data;
      console.log(data);
    })
  }

  irProductos(Categoria:any) {
    this.ctg = Categoria;
    localStorage.setItem('categoria', this.ctg);
    console.log(localStorage);
    this.router.navigateByUrl('/productos');
  }


}
