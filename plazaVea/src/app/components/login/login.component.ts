import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  dni!: string; 
  
  constructor(
    public router: Router
  ) { }

  ngOnInit(): void {
    localStorage.removeItem('dni');
  }
  login(){
    console.log(this.dni);
    if(this.dni != undefined) {
      localStorage.setItem('dni', this.dni);
      console.log(localStorage);
      this.router.navigateByUrl('/categorias');
    } 
    else {
      localStorage.removeItem('dni');
      console.log(localStorage);
      console.log(this.dni);
      this.router.navigateByUrl('/');
    }
  }

}
