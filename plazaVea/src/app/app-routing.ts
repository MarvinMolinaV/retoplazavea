import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { CategoriasComponent } from './components/categorias/categorias.component';
import { ProductosComponent } from './components/productos/productos.component';

const appRoutes = [
    { path: '', component: LoginComponent, pathMatch: "full" },
    { path: 'categorias', component: CategoriasComponent, pathMatch: "full" },
    { path: 'productos', component: ProductosComponent, pathMatch: "full" }
];

export const routing = RouterModule.forRoot(appRoutes);